#include <X11/Xlib.h>
#include <X11/Xresource.h>
#include <stdio.h>
#include <stdlib.h>

int
main()
{
    Display* dpy;

    if (!(dpy = XOpenDisplay(NULL))) {
	fprintf(stderr, "cannot open display");
	exit(1);
    }

    char* resources = XResourceManagerString(dpy);
    XrmDatabase db;
    XrmValue value;
    char* type = NULL;

    XrmInitialize();

    if (resources) {
	db = XrmGetStringDatabase(resources);

	for (int i = 0; i < 255; i++) {
	    char query[255];
	    snprintf(query, sizeof query, "*.color%d", i);
	    if (XrmGetResource(db, query, "String", &type, &value) == True) {
		printf("color%d: %s\n", i, value.addr);
	    }
	}
    }

    XCloseDisplay(dpy);

    return 0;
}
